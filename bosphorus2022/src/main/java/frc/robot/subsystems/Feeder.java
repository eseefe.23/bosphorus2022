
package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class Feeder extends SubsystemBase {
  /** Creates a new Feeder. */
  WPI_TalonSRX feederMotor = new WPI_TalonSRX(Constants.FEEDER_ID);
  
  public Feeder() {
    feederMotor.setInverted(false);
  }

  public void feedBall(){
    feederMotor.set(ControlMode.PercentOutput, 0.8);
  }
  public void retractBall(){
    feederMotor.set(ControlMode.PercentOutput, -0.6);
  }
  public void stop(){
    feederMotor.set(0.0);
  }
}