package frc.robot.subsystems;


import com.kauailabs.navx.frc.AHRS;
import com.ctre.phoenix.motorcontrol.StatusFrame;
import com.ctre.phoenix.motorcontrol.TalonSRXFeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;

import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpiutil.math.MathUtil;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

import frc.robot.RobotMap;
import frc.robot.constants.DriveConst;


public class Drive extends SubsystemBase{

    WPI_TalonSRX rightMaster = new WPI_TalonSRX(0);
    WPI_VictorSPX rightSlave1 = new WPI_VictorSPX(0);
    WPI_VictorSPX rightSlave2 = new WPI_VictorSPX(0);

    WPI_TalonSRX leftMaster = new WPI_TalonSRX(0);
    WPI_VictorSPX leftSlave1 = new WPI_VictorSPX(0);
    WPI_VictorSPX leftSlave2 = new WPI_VictorSPX(0);

    private final SpeedControllerGroup rightMotors = new SpeedControllerGroup(rightMaster, rightSlave1, rightSlave2);
    private final SpeedControllerGroup leftMotors = new SpeedControllerGroup(leftMaster, leftSlave1, leftSlave2);

    private final DifferentialDrive differentialDrive = new DifferentialDrive(leftMotors, rightMotors);

    public static final SPI.Port navX = SPI.Port.kMXP;

    AHRS navx = new AHRS(RobotMap.navX);

    private final DifferentialDriveOdometry odometry;

    PIDController turnController = new PIDController(DriveConst.kP, DriveConst.kI, DriveConst.kD);

    public Drive() {
        
        rightMaster.configSelectedFeedbackSensor(TalonSRXFeedbackDevice.CTRE_MagEncoder_Relative, 
            DriveConst.PID_LOOP_ID, DriveConst.TIMEOUT);
        
        rightMaster.setStatusFramePeriod(StatusFrame.Status_2_Feedback0, DriveConst.TIMEOUT);

        leftMaster.configSelectedFeedbackSensor(TalonSRXFeedbackDevice.CTRE_MagEncoder_Relative,
            DriveConst.PID_LOOP_ID, DriveConst.TIMEOUT);
                
        leftMaster.setStatusFramePeriod(StatusFrame.Status_2_Feedback0, DriveConst.TIMEOUT);

        turnController.enableContinuousInput(-180, 180);
        turnController.setTolerance(DriveConst.ANGLE_TOLERANCE);

        odometry = new DifferentialDriveOdometry(navx.getRotation2d());

        setMaxOutput(DriveConst.MAX_DRIVE_OUTPUT);

        resetEncoders();
    }

    @Override
    public void periodic () {

        SmartDashboard.putNumber("Right Wheel Distance", toMeters(getEncoderPos(rightMaster)));
        SmartDashboard.putNumber("Left Wheel Distance", toMeters(getEncoderPos(leftMaster)));

        SmartDashboard.putNumber("Right Wheel Velocity", toMetersPerSec(getEncoderVel(rightMaster)));
        SmartDashboard.putNumber("Left Wheel Velocity", toMetersPerSec(getEncoderVel(leftMaster)));

        odometry.update(navx.getRotation2d(), leftMaster.getSelectedSensorPosition(), rightMaster.getSelectedSensorPosition());
    }

    //
    //Movement Commands
    //

    public void setMaxOutput(double maxOutput) {
        differentialDrive.setMaxOutput(maxOutput);
    }

    public void arcadeDrive(double fwd, double rot) {
        differentialDrive.arcadeDrive(fwd, rot);
    }

    public void tankDriveVolts(double leftVolts, double rightVolts) {
        leftMotors.setVoltage(leftVolts);
        rightMotors.setVoltage(-rightVolts);
        differentialDrive.feed();
    }

    public void PIDTurn(double rotSpeed, double targetAngle) {
        arcadeDrive(rotSpeed, MathUtil.clamp(turnController.calculate(getHeading(), targetAngle), -0.8, 0.8));
    }

    //
    //Resets
    //

    public void resetOdometry(Pose2d pose) {
        resetEncoders();
        odometry.resetPosition(pose, navx.getRotation2d());
    }

    public void resetEncoders() {
        rightMaster.setSelectedSensorPosition(0);
        leftMaster.setSelectedSensorPosition(0);
    }

    public void zeroHeading() {
        navx.reset();
    }

    //
    //Getter Commands
    //

    public double getEncoderPos(TalonSRX encoder) {
        return encoder.getSelectedSensorPosition();
    }

    public double getEncoderVel(TalonSRX encoder) {
        return encoder.getSelectedSensorVelocity();
    }

    public Pose2d getPose() {
        return odometry.getPoseMeters();
      }

    public DifferentialDriveWheelSpeeds getWheelSpeeds() {
        return new DifferentialDriveWheelSpeeds(
            rightMaster.getSelectedSensorVelocity(), leftMaster.getSelectedSensorVelocity());
    }
    
    public double getAverageEncoderDistance() {
        return (getEncoderPos(rightMaster) + getEncoderPos(rightMaster) / 2.0);
    }

    public double getHeading() {
        return navx.getRotation2d().getDegrees();
    }

    public double getTurnRate() {
        return -navx.getRate();
    }

    public double toMeters(double sensorVelocity) {
        return sensorUnits * (1/4096) * (30/54) * 6 * Math.PI * (0.3048);
    }

}
