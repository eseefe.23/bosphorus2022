
package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.DoubleSolenoid;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class Intake extends SubsystemBase {
  /** Creates a new Intake. */
  WPI_TalonSRX intakeMotor = new WPI_TalonSRX(Constants.INTAKE_ID);
 

  public Intake() {
    intakeMotor.setInverted(false);
  }

  public void retractIntake(){
   
  }

  public void extendIntake(){
    
  }

  public void runForward(){
    intakeMotor.set(ControlMode.PercentOutput, 0.88);
  }

  public void runBackwards(){
    intakeMotor.set(ControlMode.PercentOutput, -0.88);
  }

  public void stop(){
    intakeMotor.set(0.0);
  }
}
