package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Feeder;
import frc.robot.subsystems.Shooter;

public class AutoFeeder extends CommandBase {
  /** Creates a new AutoConveyor. */
  private Shooter shooter;
  private Feeder feeder;
  private int RPM;
  private boolean isShooterReady= false;
  
  public AutoFeeder(Shooter shooter, Feeder feeder, int RPM) {
    this.shooter = shooter;
    this.feeder = feeder;
    this.RPM = RPM;
    addRequirements(this.shooter, this.feeder);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    shooter.setRPM(RPM);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    // Button binding
    if(shooter.shooterPID.atSetpoint()) {
      feeder.feedBall();
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    feeder.stop();
    shooter.stop();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}