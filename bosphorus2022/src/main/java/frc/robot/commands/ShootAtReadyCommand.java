package frc.robot.commands;

import edu.wpi.first.math.geometry.Rotation2d;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.Feeder;
import frc.robot.subsystems.Shooter;

public class ShootAtReadyCommand extends CommandBase {

  private Feeder feeder;
  private Shooter shooter;
  private int rpm;
  private int iteration = 0;
  private Drivetrain drivetrain;

  /** Creates a new ShootWhenReadyCommand. */
  public ShootAtReadyCommand(Conveyor conveyor, Shooter shooter, Drivetrain drivetrain) {
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(feeder, shooter, drive);
    this.drivetrain = drive;
    this.feeder = feeder;
    this.shooter = shooter;
    this.rpm = (int) SmartDashboard.getNumber("target shooter RPM", Constants.SHOOT_RPM);
  }
  
  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    feeder.stop();
    shooter.setRPM(rpm);
    this.rpm = (int) SmartDashboard.getNumber("target shooter RPM", Constants.SHOOT_RPM);
  }
  
  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    
    
    shooter.setRPM(rpm);
    
    if (shooter.shooterPID.atSetpoint()) {
      iteration++;
      if(iteration>5){
        
        feeder.feedBall();
      }
    } else {
      feeder.stop();
      iteration = 0;
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    feeder.stop();
    shooter.stop();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}