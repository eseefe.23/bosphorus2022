package frc.robot.commands.Drive;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.Drivetrain;

public class TurnToAgnle extends CommandBase {
  /** Creates a new TurnToAgnle. */
  Drivetrain drivetrain;
  PIDController turnPID = new PIDController(0.0075, 0, 0);

  double angleSetpoint;
  
  public TurnToAgnle(Drivetrain drivetrain, double angleSetpoint) {
    this.drivetrain = drive;
    this.angleSetpoint = angleSetpoint;
    addRequirements(drive);
    turnPID.setTolerance(2);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  @Override
  public void execute() {
    // ! NOT FIELD RELATIVE
    drivetrain.drive(0, 0, 
    turnPID.calculate(drivetrain.getGyroDouble(), angleSetpoint) * Constants.Drivetrain.kMaxAngularSpeed
    , false);
  }

  @Override
  public boolean isFinished() {
    return false;
  }
}