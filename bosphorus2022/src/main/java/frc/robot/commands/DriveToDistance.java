package frc.robot.commands;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.subsystems.Drivetrain;

public class DriveToDistance extends SubsystemBase {
  /** Creates a new DriveToDistance. */
  Drivetrain drivetrain;

  double setpoint = 100;
  double output;

  double kp;
  PIDController distancePID = new PIDController(kp, 0, 0);
  public DriveToDistance(Drivetrain drive) {
    this.drivetrain = drive;
  }

  @Override
  public void periodic() {
    drivetrain.drive(0, output, 0, true);
  }
}