package frc.robot;

import java.lang.invoke.ConstantCallSite;
import java.util.function.BooleanSupplier;

import edu.wpi.first.math.controller.ProfiledPIDController;
import edu.wpi.first.math.geometry.Pose2d;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.PrintCommand;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.button.Button;
import edu.wpi.first.wpilibj2.command.button.XboxControllerButton;

import frc.robot.commands.CollectCargoCommand;
import frc.robot.commands.RGBCommand;
import frc.robot.commands.ShootWhenReadyCommand;
import frc.robot.commands.StorageCommand;
import frc.robot.commands.ThePoPo;

import frc.robot.lib.drivers.WS2812Driver;

import frc.robot.subsystems.Feeder;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.Shooter;

import frc.robot.subsystems.Drivetrain;

public class RobotContainer{

 Drivetrain drivetrain = new Drivetrain();
 Shooter shooter = new Shooter();
 Feeder feeder = new Feeder();
 Intake intake = new Intake();
 Elevator elevator = new Elevator(); 

 XboxController driver = new XboxController(0);
 XboxController operator = new XboxController(1);

 CollectCargoCommand collectCargoCommand = new CollectCargoCommand(intake, feeder);
 ShootAtReadyCommand shootAtReadyCommand = new ShootAtReadyCommand(feeder, shooter, drivetrain);

 SendableChooser<Command> autonomous_chooser = new SendableChooser<>();

 public RobotContainer(){
    configureButtonBindings();
    autonomousChooser.setDefaultOption("Empty", new PrintCommand("Zort!!"));
    autonomousChooser.addOption("Three balls", new AutoThreeBalls(swerveDrivetrain, conveyor, shooter, intake, storage));
    autonomousChooser.addOption("Five balls", new AutoFiveBalls(swerveDrivetrain, conveyor, shooter, intake, storage));
 }
 private void configureButtonBindings(){
     drivetrain.setDefaultCommand(drivecommand);
     elevator.setDefaultCommand(elevatorcommand);
     Button resetOdometryButton = new XboxControllerButton(driver, 2);
     resetOdometryButton.whenPressed(new InstantCommand(() -> {
     drivetrain.resetOdometry(new Pose2d());
     }
    ) 
   );
    
    Button [] feederbuttons = {
        new XboxControllerButton(operator,7),
        new XboxControllerButton(operator,8),
        new XboxControllerButton(operator,9)
    };
    feederbuttons[0].whileHeld(AutoFeeder);
    feederbuttons[1].whileHeld(new RunCommand(()-> feeder.retractBall, feeder)).whenReleased(new RunCommand(()-> feeder.stop(),feeder));
   
    Button [] intakebuttons = {
        new XboxControllerButton(operator,1),
        new XboxControllerButton(operator,2),
        new XboxControllerButton(operator,3)
    };
    intakebuttons[0].whileHeld(CollectCargoCommand);
    intakebuttons[1].whileHeld(new RunCommand(()-> intake.runBackwards, intake)).whenReleased(new RunCommand(()-> intake.stop(),intake));
    
    Button [] shooterbuttons = {
        new XboxControllerButton(operator, 4),
        new XboxControllerButton(operator, 5),
        new XboxControllerButton(operator, 6)
    };

    shooterbuttons[0].whileHeld(new RunCommand(()->shooter.setRPM(5000), shooter))
      .whenReleased(new RunCommand(()-> shooter.stop(), shooter));

    shooterbuttons[1].whileHeld(shootWhenReadyCommand);



  }
 public Command getAutonomousCommand(){
    return autonomousChooser.getSelected();
 }
}